/**
 * @author csblo
 * @licence MIT
 * @file scene3d.js
 * @description work base for creating 3d application
 */

var Scene3D = function ()
{
    var self = this;
    var container;
    var camera, scene, renderer, controls;

    this.init = function()
    {

        setupCamera();
        setupScene();
        setupRenderer();

        setupControls();
        setupLights();
        setupGrid(200, 20);

        window.addEventListener( 'resize', onWindowResize, false );

    };

    function setupCamera()
    {
        //Create camera and set position
        camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 1, 1000);

        camera.position.x = 200;
        camera.position.y = 100;
        camera.position.z = 200;
    }

    function setupScene()
    {
        //Create scene and set background color
        scene = new THREE.Scene();
        scene.background = new THREE.Color(0x222222);
    }

    function setupRenderer()
    {
        //Create container
        container = document.createElement( 'div' );
        document.body.appendChild( container );

        //Create webgl renderer and add to container
        renderer = new THREE.WebGLRenderer();
        renderer.setClearColor( 0x000000 );
        renderer.setPixelRatio( window.devicePixelRatio );
        renderer.setSize( window.innerWidth, window.innerHeight );
        container.appendChild( renderer.domElement );
    }

    function setupLights()
    {
        var ambientLight = new THREE.AmbientLight( 0xffffff );
        ambientLight.intensity = 0.6;
        scene.add( ambientLight );

        var directionalLight = new THREE.DirectionalLight( 0xffffff, 0.5 );
        directionalLight.position.x = Math.random() - 0.5;
        directionalLight.position.y = Math.random() - 0.5;
        directionalLight.position.z = Math.random() - 0.5;
        directionalLight.position.normalize();
        scene.add( directionalLight );

        var directionalLight = new THREE.DirectionalLight( 0xffffff, 0.5 );
        directionalLight.position.x = Math.random() - 0.5;
        directionalLight.position.y = Math.random() - 0.5;
        directionalLight.position.z = Math.random() - 0.5;
        directionalLight.position.normalize();
        scene.add( directionalLight );

        var pointLight = new THREE.PointLight(0xffffff, 1, 1000);
        pointLight.position.set(0,100,0);
        camera.add(pointLight);
    }

    function setupGrid(size, division)
    {
        var grid = new THREE.GridHelper(size, division);
        scene.add(grid);
    }

    function setupControls()
    {
        controls = new THREE.OrbitControls( camera, renderer.domElement );
        controls.enableDamping = false;
        controls.enableZoom = true;
        controls.enablePan = true;
    }

    function onWindowResize()
    {
        renderer.setSize( window.innerWidth, window.innerHeight );
        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();
    }

    this.animate = function()
    {
        requestAnimationFrame( self.animate );

        controls.update();
        render();
    };

    function render()
    {
        renderer.render( scene, camera );
    }

};
